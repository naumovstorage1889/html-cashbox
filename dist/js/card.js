

//gallery
var galleryCurrent = document.querySelector('.gallery_current img');
var galleryItem = document.querySelectorAll('.gallery_set img');

for (var i = 0; i < galleryItem.length; i++) {
	galleryItem[i].addEventListener('click', function (e) {
		if (e.target.src == galleryCurrent.src) {
			return false
		} else {
			for (var a = 0; a < galleryItem.length; a++) {
				galleryItem[a].classList.remove('active');
			};
			galleryCurrent.animate([
				{
					opacity: '1'
				},
				{
					opacity: '.3'
				}
			], 150);
			setTimeout(function () {
				e.target.classList.add('active');
				galleryCurrent.src = e.target.src;
			}, 100);

			console.log(i)
		}
	})
};


//brief popup

document.querySelector('.brief_popup__open').addEventListener('click', function () {
	this.closest('.brief_popup').classList.add('active');
});
document.querySelector('.brief_popup__close').addEventListener('click', function () {
	this.closest('.brief_popup').classList.remove('active');
});

//bill
var cardOptionNumber = document.querySelectorAll('.card_option__price');
var cardOptionInput = document.querySelectorAll('.card_option input');
var cardOptionOutput = document.querySelector('.card_option__sum .numer');

getBill();

for (var i = 0; i < cardOptionInput.length; i++) {
	cardOptionInput[i].addEventListener('change', getBill)
};

function getBill() {
	var bill = 0;
	for (var i = 0; i < cardOptionNumber.length; i++) {
		if (cardOptionInput[i].checked) {
			bill += parseInt(cardOptionNumber[i].innerHTML.replace(/\s{1,}/g, ''));
		}
	};

	var billArray = String(bill).split('');
	switch (billArray.length) {
		case 4:
			billArray.splice(1, 0, ' ');
			break;
		case 5:
			billArray.splice(2, 0, ' ');
			break;
		case 6:
			billArray.splice(3, 0, ' ');
			break;
		case 7:
			billArray.splice(1, 0, ' ');
			billArray.splice(5, 0, ' ');
			break;
	}
	var billString = '';
	for (var i = 0; i < billArray.length; i++) {
		billString += billArray[i];
	};

	cardOptionOutput.innerHTML = billString
};




