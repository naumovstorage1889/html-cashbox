var tab = function() {

	var mh = 0;
	$(".tab_content__item").each(function () {
		var h_block = parseInt($(this).height());
		if(h_block > mh) {
			mh = h_block;
		};
	});
	$(".tab_content__item").height(mh);

	$('.tab_content__item').not('.active').hide();
	
	$('.tab_title__item').click(function() {
		var titleItem = $(this);
		var wrap = titleItem.parents('.tab_wrap');
		var contentItemCurrent = wrap.find($('.tab_content__item')).eq(titleItem.index());

		titleItem.addClass('active');
		wrap.find($('.tab_title__item')).not(titleItem).removeClass('active');

		contentItemCurrent.addClass('active');
		wrap.find($('.tab_content__item')).not(contentItemCurrent).removeClass('active');

		contentItemCurrent.show();
		wrap.find($('.tab_content__item')).not(contentItemCurrent).hide();
	});

};